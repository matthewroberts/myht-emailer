<?php
    // By the time I was done writing this code, I came across the "provider website" (myhealthtrack\app\MyHealthTrackDoc).
    // Maybe this code should be there, but I would need more background into the purpose of the provider website to 
    // see if this code would fit in there. I tried accessing that website but I could not login to see what's in there.
    include "crowdhub_entry.php";

    // This functionality can be called from a cloud Job Scheduler every Monday, so anonymous access is 
    // allowed. An IP access list is used for access control. The IP addresses should be moved to a configuration so that it
	// can be dynamic
    $whitelist = array('111.111.111.111', '112.112.112.112', '127.0.0.1', '189.158.213.17');
    if (!(in_array($_SERVER['REMOTE_ADDR'], $whitelist))) {
        exit;
    }

    $mAppInformation = $mCrowdHubServices->getAppInformation();
    $mandrill = new Mandrill($mAppInformation["EMAIL_MANDRILLAPI"]);

    $providers = DB::query("SELECT crowdhub_content.content_id,crowdhub_content.content_name FROM crowdhub_content_to_buckets INNER JOIN crowdhub_content ON crowdhub_content_to_buckets_content=content_id WHERE crowdhub_content_to_buckets_bucket IN ( SELECT bucket_id FROM crowdhub_buckets WHERE bucket_name IN (%s))", "Provider");
    foreach($providers as $provider)
    {
        // getting the date range
        $monday = date("Y-m-d", strtotime("last week monday"));
        // can't do 'last monday' because I'll get the incorrect date if this code is run on a Monday
        $sunday = date("Y-m-d", strtotime("last sunday + 1 day"));

        // TODO I believe the provider email is not being tracked in the database, so I left the recipient as empty for now. It should be filled out with something like $provider["email"]
        $to = "";
        $txt = "Hello ".$provider["content_name"].", these are your weekly analytics behavior!<div>&nbsp;</div>";

        // Get number of unique logins per day
        $uniqueLogins = DB::query("SELECT CAST(crowdhub_activity.activity_date AS DATE) as date, count(DISTINCT activity_user) as users FROM crowdhub_activity join crowdhub_activity_types on crowdhub_activity.activity_type = crowdhub_activity_types.type_id join users_to_providers on crowdhub_activity.activity_user = users_to_providers.user_id WHERE users_to_providers.provider_id = %i AND crowdhub_activity.activity_date >= %t AND crowdhub_activity.activity_date < %t GROUP BY CAST(activity_date AS DATE) ORDER BY CAST(activity_date AS DATE)", $provider["content_id"], $monday, $sunday);
        $txt .= buildGenericTable("Day", "Unique Logins", "date", "users", $uniqueLogins);

        // Get number of sessions per day
        $numberOfSessions = DB::query("SELECT CAST(crowdhub_activity.activity_date AS DATE) as date, count(DISTINCT activity_user) as users FROM crowdhub_activity join crowdhub_activity_types on crowdhub_activity.activity_type = crowdhub_activity_types.type_id join users_to_providers on crowdhub_activity.activity_user = users_to_providers.user_id WHERE users_to_providers.provider_id = %i AND crowdhub_activity.activity_date >= %t AND crowdhub_activity.activity_date < %t AND activity_value_string = 'Authenticated' GROUP BY CAST(activity_date AS DATE) ORDER BY CAST(activity_date AS DATE)", $provider["content_id"], $monday, $sunday);
        $txt .= buildGenericTable("Day", "Number of sessions", "date", "users", $numberOfSessions);
       
        // Get patient/activity data like this: 27 patients completed 210 recovery activities (ignoring 'Authenticated' value)
        $activities = DB::query("SELECT CAST(crowdhub_activity.activity_date AS DATE) as date, count(DISTINCT activity_user) as users, count(*) as activities FROM crowdhub_activity join crowdhub_activity_types on crowdhub_activity.activity_type = crowdhub_activity_types.type_id join users_to_providers on crowdhub_activity.activity_user = users_to_providers.user_id WHERE users_to_providers.provider_id = %i AND crowdhub_activity.activity_date >= %t AND crowdhub_activity.activity_date < %t AND activity_value_string != 'Authenticated' GROUP BY CAST(activity_date AS DATE) ORDER BY CAST(activity_date AS DATE)", $provider["content_id"], $monday, $sunday);
        $txt .= buildActivityTable("Day", "Activities", "date", "users", "activities", $activities);
        
        $message = array(
			'subject' => "Weekly Analytics",
			'html' => $txt,
			'from_email' => mAppInformation["EMAIL_SENDER"],
			'from_name' => mAppInformation["EMAIL_SENDER"],
			'to' => $to,
			'track_opens' => false,
			'track_clicks' => false,
			'preserve_recipients'=>false,
			'merge' => false
        );
        $result = $mandrill->messages->send($message);
     }

     function buildGenericTable($columnKey, $columnValue, $recordKey, $recordValue, $data) {
        $txt = "<table style='border-collapse: collapse;border: 1px solid black;'><tr><th style='border: 1px solid black;'>".$columnKey."</th><th style='border: 1px solid black;'>".$columnValue."</th></tr>";

        foreach($data as $record)
        {
            $txt .= "<tr><td style='border: 1px solid black;'>".$record[$recordKey]."</td><td style='border: 1px solid black;'>".$record[$recordValue]."</td></tr>\n";
        }

        $txt .="</table><div>&nbsp;</div>";
        return $txt;
     }

     function buildActivityTable($columnKey, $columnValue, $recordKey, $recordPatients, $recordActivities, $data) {
        $txt = "<table style='border-collapse: collapse;'><tr><th style='border: 1px solid black;'>".$columnKey."</th><th style='border: 1px solid black;'>".$columnValue."</th></tr>";

        foreach($data as $record)
        {
            $txt .= "<tr><td style='border: 1px solid black;'>".$record[$recordKey]."</td><td style='border: 1px solid black;'>".$record[$recordPatients]." patients completed ".$record[$recordActivities]." recovery activities</td></tr>\n";
        }

        $txt .="</table><div>&nbsp;</div>";
        return $txt;
     }
?>
